import React, { Component } from "react";

import "./App.css";

class App extends Component {
  state = {
    user: "",
    email: "",
    pass: "",
    consent: false,

    errors: {
      user: false,
      email: false,
      pass: false,
      consent: false
    }
  };

  messages = {
    username_incorrect: "Nazwa musi być dłuższa niż 5 znaków",
    email_incoreect: "Brak @ w adresie",
    password_incorrect: "Hasło musi mieć minimum 8 znaków",
    consent_incorrect: "Zgoda nie została potwierdzona"
  };

  handleChange = e => {
    const name = e.target.id;
    if (e.target.type !== "checkbox") {
      this.setState({
        [name]: e.target.value
      });
    } else {
      this.setState({
        [name]: !this.state.consent
      });
    }
  };
  handleSubmit = e => {
    e.preventDefault();
    const validation = this.formValidation();
    // console.log(validation)
    if (validation.correct) {
      this.setState({
        user: "",
        email: "",
        pass: "",
        consent: false,
        message: "Formularz został wysłany",

        errors: {
          user: false,
          email: false,
          pass: false,
          consent: false
        }
      });
    } else {
      this.setState({
        errors: {
          user: !validation.user,
          email: !validation.email,
          pass: !validation.pass,
          consent: !validation.consent
        }
      });
    }
  };
  formValidation = () => {
    let user = false;
    let email = false;
    let pass = false;
    let consent = false;
    let correct = false;

    if (this.state.user.length > 5 && this.state.user.indexOf(" ") === -1) {
      user = true;
    }
    if (this.state.email.indexOf("@") !== -1) {
      email = true;
    }
    if (this.state.pass.length > 8) {
      pass = true;
    }
    if (this.state.consent) {
      consent = true;
    }
    if (user && email && pass && consent) {
      correct = true;
    }
    return {
      user,
      email,
      pass,
      consent,
      correct
    };
  };
  componentDidUpdate() {
    if (this.state.message !== "") {
      setTimeout(() => {
        this.setState({
          message: ""
        });
      }, 3000);
      console.log("update");
    }
  }

  render() {
    return (
      <div className="form">
        <form onSubmit={this.handleSubmit} noValidate>
          <label htmlFor="user">
            Twoje imię:{" "}
            <input
              type="text"
              id="user"
              placeholder="Imię"
              value={this.state.user}
              onChange={this.handleChange}
            />
            {this.state.errors.user && (
              <span>{this.messages.username_incorrect}</span>
            )}
          </label>
          <label htmlFor="email">
            Twój e-mail:
            <input
              type="email"
              id="email"
              placeholder="adres@mailowy"
              value={this.state.email}
              onChange={this.handleChange}
            />
            {this.state.errors.email && (
              <span>{this.messages.email_incoreect}</span>
            )}
          </label>
          <label htmlFor="pass">
            Twóje hasło:
            <input
              type="password"
              id="pass"
              placeholder="Hasło"
              value={this.state.password}
              onChange={this.handleChange}
            />
            {this.state.errors.pass && (
              <span>{this.messages.password_incorrect}</span>
            )}
          </label>
          <label htmlFor="consent">
            <input
              type="checkbox"
              id="consent"
              checked={this.state.checked}
              onChange={this.handleChange}
            />
            Wyrażam zgodę na blablabla
            {this.state.errors.consent && (
              <span>{this.messages.consent_incorrect}</span>
            )}
          </label>
          <input
            type="submit"
            value="Wyślij formularz"
            onClick={this.handleSubmit}
          />
        </form>
        {this.state.message && <h3>{this.state.message}</h3>}
      </div>
    );
  }
}

export default App;
